#include "Expression.h"
#include <string.h>
#include <string>
#include <sstream>
#include <iostream>
using namespace std; 

#include "Expression.h"


// pour la class Expression : 

int Expression::instances = 0;

Expression::Expression() {instances += 1;}// compteur d'instances crées. 

Expression::~Expression() {
	instances -= 1;// destructeur
}

std::ostream& operator <<(std::ostream &out, const Expression &expr) {
	return expr.affiche(out);
}
Expression * Expression::simplifier() const {
  return clone();
}
// pour la class Nombre :

Nombre::Nombre(int value): value_(value){}
Nombre::Nombre() : Nombre(0){}
std::ostream & Nombre::afficher(std::ostream & out) const {
  out << value_;
  return out;
}
Nombre * Nombre::derive(const std::string & var) const {
  return new Nombre();
}
Nombre * Nombre::clone() const {
  return new Nombre(value_);
}
Nombre * Nombre::simplifier() const {
  return this->clone();
}


//pour la class Variable :

Variable::Variable(std::string name): name_(name) {}
std::ostream & Variable::afficher(std::ostream & out) const {
  out << name_;
  return out;
}
Nombre * Variable::derive(const std::string & var) const {
  if (var == name_){return new Nombre(1);}
  else {return new Nombre();}
}

Variable * Variable::clone() const {
  return new Variable(name_);
}
Variable * Variable::simplifier() const {
  return this->clone();
}
//pour la class Operation :

Operation::Operation(const Expression * ex_1, const Expression * ex_2, std::string symb): symb_(symb) {
  ex_g = ex_1->clone(); ex_d = ex_2->clone();
}
Operation::~Operation() {delete ex_g; delete ex_d;}// supression des copies. 

std::ostream & Operation::afficher(std::ostream & out) const {
  out << "[";
  ex_g->affiche(out);
  out << symbole();
  ex_d->affiche(out);
  out << "]";
  return out;
}
std::string Operation::symbole() const {
  return symb_;
}


//Méthodes relatives à Addition

Addition::Addition(const Expression * ex_1, const Expression * ex_2): Operation(ex_1, ex_2, " + "){}

Addition * Addition::clone() const {
  return new Addition(ex_g->clone(), ex_d->clone());
}

Addition * Addition::derive(const std::string & var) const {
  return new Addition(ex_g->derive(var), ex_d->derive(var));
}
Expression * Addition::simplifier() const {
  std::ostringstream output1;
  std::ostringstream output2;

  Expression * simp_ex_g = ex_g->simplifier();
  Expression * simp_ex_d = ex_d->simplifier();

  output1 << *(simp_ex_g);
  if (output1.str() == "0"){
    return simp_ex_d;
  }
  
  output2 << *(simp_ex_d);
  if (output2.str() == "0"){
    return simp_ex_g;
  }

  return this->clone();
// pour la class Multiplication : 

Multiplication::Multiplication(const Expression * ex_1, const Expression * ex_2): Operation(ex_1, ex_2, " * "){}

Multiplication * Multiplication::clone() const {
  return new Multiplication(ex_g->clone(), ex_d->clone());
}

Addition * Multiplication::derive(const std::string & var) const {
  return new Addition(
    new Multiplication(ex_g->derive(var), ex_d->clone()), 
    new Multiplication(ex_g->clone(), ex_d->derive(var)));
}
Expression * Multiplication::simplifier() const {
  std::ostringstream output1;
  std::ostringstream output2;

  Expression * simp_ex_g = expr_g->simplifier();
  Expression * simp_ex_d = expr_d->simplifier();
  output1 << *(simp_ex_g);
  output2 << *(simp_ex_d);

  if (output1.str() == "0" or output2.str() == "0"){
    return new Nombre();
  }
  if (output1.str() == "1"){
    return simp_ex_d;
  }
  if (output2.str() == "1"){
    return simp_ex_g;
  }
  return this->clone();
  } 





 
