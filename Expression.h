#ifndef EXPRESSION_H_
#define EXPRESSION_H_

#include <iostream>
#include <string>
// class Expression : 

class Expression {
public:
  Expression();
  virtual ~Expression();
	virtual Expression * derive(const std::string & var) const = 0;
	virtual std::ostream & affiche(std::ostream & out) const = 0;
	virtual Expression * clone() const = 0;
  virtual Expression * simplifier() const;
  static int instances;
};

  std::istream & operator >> (std::ostream & in, const Expression & expr);
  std::ostream & operator << (std::ostream & out, const Expression & expr);

// Class Nombre : 
  class Nombre: public Expression { 
    public:
    Nombre();
    Nombre(int value);
    std::ostream & afficher(std::ostream & out) const override;
    Nombre * derive(const std::string & var) const override;
    Nombre * clone() const override;
    Nombre * simplifier() const override;
     private:
    const int value_;  
    };

// Class Variable : 
  class Variable: public {
    public:
      Variable(std::string name);
      std::ostream & afficher(std::ostream & out) const override;
      Nombre * derive(const std::string & var) const override;
      Variable * clone() const override;
      Variable * simplifier() const override;
    private:
      const std::string name_;  

  };
// Class opération : 
  class Operation: public Expression
  {
  public:
    Operation(const Expression * ex_1, const Expression * ex_2, std::string symb);
    ~Operation();
    std::string symbole() const;
    std::ostream & afficher(std::ostream & out) const override;
  protected:
    const Expression * ex_g;
    const Expression * ex_d;
  private:
    const std::string symb_;
  };

 // Les classes addition et multiplication : *

class Addition: public Operation{
  public:
    Addition(const Expression * ex_1, const Expression * ex_2);
    Addition * derive(const std::string & var) const override;
    Addition * clone() const override;
    Expression * simplifier() const override;
    
};

  class Multiplication: public Operation{
  public:
    Multiplication(const Expression * ex_1, const Expression * ex_2);
    Addition * derive(const std::string & var) const 
    override;
    Multiplication * clone() const override;
    Expression * simplifier() const override;
};

#endif